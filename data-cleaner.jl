using DataFrames
using XLSX
# variable setting
#df = DataFrame(A = 1:5, B = ["A", "B", "C", "D", "E"])
directorio = "/data/data.xlsx"
#rawdata = XLSX.readxlsx(data)
#sheet = rawdata["Respuestas de formulario 1"]
tablaBase = DataFrame(XLSX.readtable(directorio, "Respuestas de formulario 1")...)
#celda1= "Lazo (10); libertad (9); corazón (8); llama (7); pasión (6); verdad (5); voluntad (4); cariño (3); fortaleza (2); virtud (1);"
#celda2= "Cariño (10); Amor (9); Interés (8); Reciprocidad (7); Confianza (6); Felicidad (5); Experiencia (4); Apego (3); Libertad (2); detalle (1)"
#celda3="Querer (10); Cariño (9); Vínculo (8); Confianza (7); Respeto (6); Reciprocidad (5); Bello (4); Doloroso (3); Importante (2); Necesario (1) "
#coordenadas iniciales para nuestros datos
y=1 #hasta 20
x=4 #hasta 6

celda = tablaBase[y,x]

println(celda)

columnas = []

#Functions
function recorridoCeldas(celda::String)
    println("FOR LOOP por celda")
    println("Requiere coordenadas de la celda")
    concepto = ""
    Are = String[]
    for u in 1:9
        y1=findlast("$u", celda)
        #println(y1)
        v=u+1
        #println(v)
        y2=findfirst("$v", celda)
        #println(y2)
        #println(y1.start)
        #println(y2.start)
        concepto = strip(SubString(celda,y2.start+4,y1.start-1), [' ', ')', '(', ';']) # +4 y -1 son por el número de caracteres
        #println(concepto)
        #println(length(concepto))
        push!(Are,concepto)
    end
    y1=findfirst("10", celda)
    concepto = strip(SubString(celda,1,y1.start-1), [' ', ')', '(', ';'])
    #println(concepto)
    #println(length(concepto))
    push!(Are,concepto)
    #reverse!(Are)
    #println("")
    println(Are)
    return Are
end

#Función para contar la lista debe encontrar todos los tipos de palabra y después listarlos en varios
#Y diferentes ordenes dependiendo de el tipo de grupo que se busque
#Funcion 1 para encontrar todas las palabras tentativamente recursiva que compare todas las palabras que hay en la tabla hasta que no encuentre nuevas
#Funcion 2 Hace un conteo general de todas las palabras encontradas por la lista de palabras únicas (puede que también podamos hacer un conteo mientras se encuentran todas las palabras)
#Funcion xx manejo de datos con estas palabras encontradas
for y in 3:4
    for x in 4:6
        celda = tablaBase[y,x]
        columnaNueva = recorridoCeldas(celda)
        push!(columnas,columnaNueva)
        #puede hacerse que exporte e importe sexo como cabezal tambien que muestre a que pregunta se refiere en que linea
    end
end

println()
println(columnas)

tabla2 = DataFrame(columnas, :auto)

println(tabla2)

#Export
#New one because there is not worth to edit the original file
XLSX.writetable("/data/datos.xlsx", Datos_Enlistados = (collect(DataFrames.eachcol(tabla2)), DataFrames.names(tabla2)))

#celda = sheet[y,x]
#XLSX.writetable("report.xlsx", REPORT_A=( collect(DataFrames.eachcol(df1)), DataFrames.names(df1) ))