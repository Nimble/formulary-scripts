# formulary-scripts

Data management

Este programa utiliza y esta escrito en **julia** (entonces necesitas descargar este programa). Importa o depende de los paquetes de manejo de datos en excel pues lo que busca es limpiar u organizar los datos de las tablas generadas por algún formulario.

## Dependencias

- DataFrames
- XLSX

Una vez instaladas hay un wrapper para sistemas Unix (Linux/Mac) el que simplemente puedes ejecutar sin más

- [ ] tutorial como instalar dependencias
- [ ] ir en más detalle sobre su uso así como lo que hace o realiza
- [ ] Mejorar la interactibilidad con el código
- [ ] Dividir el script entre distintos que realizan distintas operaciones, por ejemplo un script distinto para graficar